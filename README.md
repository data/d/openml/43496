# OpenML dataset: League-of-Legends-SOLO-Q-Ranked-Games

https://www.openml.org/d/43496

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
League of Legends is a MOBA (multiplayer online battle arena) where 2 teams (blue and red) face off. There are 3 lanes, a jungle, and 5 roles. The goal is to take down the enemy Nexus to win the game.
Content
This dataset contains stats of approx. 25000 ranked games (SOLO QUEUE) from a Platinium ELO.
Each game is unique. The gameId can help you to fetch more attributes from the Riot API.
Each game has features from different time frames from 10min to the end of the game. For example, game1 10min, game1 12min, game1 14min etc.
In total there are +240000 game frames.  
There are 55 features collected for the BLUE team  . This includes kills, deaths, gold, experience, level It's up to you to do some feature engineering to get more insights.
The column hasWon is the target value if you're doing classification to predict the game outcome.
Otherwise you can use the gameDuration attribute if you wanna predict the game duration.
Attributes starting with is* are boolean categorial values (0 or 1).
Happy hacking.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43496) of an [OpenML dataset](https://www.openml.org/d/43496). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43496/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43496/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43496/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

